unit TelaCadastro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ComCtrls, Vcl.Mask;

type
  TForm1 = class(TForm)
    Edit1: TEdit;
    Edit3: TEdit;
    Edit4: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit5: TEdit;
    Button1: TButton;
    StatusBar1: TStatusBar;
    Edit6: TEdit;
    Label6: TLabel;
    Button3: TButton;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    MaskEdit4: TMaskEdit;
    MaskEdit1: TMaskEdit;
    MaskEdit2: TMaskEdit;
    MaskEdit3: TMaskEdit;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure Button1Click(Sender: TObject);
    procedure Edit5Enter(Sender: TObject);
    procedure Edit5Exit(Sender: TObject);
    procedure Button3Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
var
  cor       : Tcolor;
  bolValida : Boolean;
begin
  cor       := clCream;
  bolValida :=  true;

  if trim(edit1.Text) = '' then
    begin
      bolValida   :=  false;
      edit1.Color :=  cor;
  end;

  if trim(MaskEdit1.Text) = '' then
    begin
    //  showmessage('O campo nome � de preenchimento obrigat�rio');
      bolValida := false;
      MaskEdit1.Color :=  clCream;
  end;

  if trim(edit3.Text) = '' then
    begin
    //  showmessage('O campo nome � de preenchimento obrigat�rio');
      bolValida   := false;
      edit3.Color :=  clCream;
  end;

  if bolValida then
    begin
      showmessage('Cadatro realizado com sucesso!');
    end
  else
    begin
      showmessage('Preencha os campos obrigat�rios!');
  end;

end;

procedure TForm1.Button3Click(Sender: TObject);
begin
  application.Terminate;
end;

procedure TForm1.Edit5Enter(Sender: TObject);
begin
  statusbar1.Panels[0].Text := 'Informe somente n�meros';
end;

procedure TForm1.Edit5Exit(Sender: TObject);
begin
   statusbar1.Panels[0].Text := '';
end;

procedure TForm1.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    if key = 13 then
       begin
         Perform(Wm_NextDlgCtl,0,0);
       end
    else if key = 27 then
      begin
          Button3Click(self);
      end

    else if key = 112 then
        begin
          Button1Click(self);
    end;

end;

procedure TForm1.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
    begin
      key :=  #0;
  end;
end;

end.
